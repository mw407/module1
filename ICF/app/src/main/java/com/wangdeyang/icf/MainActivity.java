package com.wangdeyang.icf;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.*;
public class MainActivity extends AppCompatActivity {
//    TextView info = findViewById(R.id.info);
    private static final String url = "jdbc:mysql:vcm-7996.vm.duke.edu:3306/icf_info";
    private static final String user = "icf_user";
    private static final String password = "12345";
    private int i = 0;
    private int[] id;
    private String[] name;
    // Establish connection to MySQL
    protected void ConnectDatabase()

    {
        try {
            Connection conn = DriverManager.getConnection(url, user, password);
            Statement st = conn.createStatement();
            String sql = "insert into user_info(id,name) values(2,'ddl')";
            final ResultSet rs = st.executeQuery(sql);

            while(rs.next()){
                i++;
            }
            //Set Array size
            id = new int[i];
            name = new String[i];
            i=0;

            rs.first();

            //Store data to array
            while(rs.next()){
                id[i] = rs.getInt(1);
                name[i] = rs.getString(2);
                i++;
            }

            conn.close();


        }
        catch(SQLException e){
            System.out.println("failed open database");
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        configureNextButton();
    }


    private void configureNextButton() {
        Button nextButton = findViewById(R.id.btnDoMagic);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, EduActivity.class));
            }

        });

    }
}
